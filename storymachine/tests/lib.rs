extern crate storymachine;

use storymachine::StoryMachine;

#[test]
fn no_input() {
	let mut story_machine = StoryMachine::new();
	let story = story_machine.generate_story(2);
	assert_eq!(story, None);	
}

#[test]
fn blank_input() {
	let mut story_machine = StoryMachine::new();
	story_machine.import_text("".to_string());
	let story = story_machine.generate_story(2);
	assert_eq!(story, None);	
}

#[test]
fn not_enough_input() {
	let mut story_machine = StoryMachine::new();
	story_machine.import_text("one two".to_string());
	story_machine.import_text("aey bey".to_string());
	let story = story_machine.generate_story(2);
	assert_eq!(story, None);	
}

#[test]
fn finish_a_trigram_in_two_imports() {
	let mut story_machine = StoryMachine::new();
	story_machine.import_text("one two".to_string());
	story_machine.import_text("one two three".to_string());
	let story = story_machine.generate_story(2);
	assert!(story != None);	
}

#[test]
fn first_word_capital() {
	let mut story_machine = StoryMachine::new();
	story_machine.import_text("one Two three four".to_string());
	if let Some(story) = story_machine.generate_story(2) {
		if let Some(first_word) = story.split_whitespace().next() {
			assert_eq!(first_word.to_string(), "Two".to_string());
		}
		else { 
			assert!(false);
		}
	}
	else {
		assert!(false);
	}
}

#[test]
fn last_word_punctuated() {
	let mut story_machine = StoryMachine::new();
	story_machine.import_text("one Two three".to_string());
	if let Some(story) = story_machine.generate_story(2) {
		let chars: Vec<_> = story.char_indices().collect();
		assert_eq!(chars[chars.len()-3].1, '.');
		assert_eq!(chars[chars.len()-2].1, '\n');
		assert_eq!(chars[chars.len()-1].1, '\n');
	}
	else {
		assert!(false);
	}
}

