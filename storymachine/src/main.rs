extern crate storymachine;

use std::env;
use std::io::prelude::*;
use std::fs::File;
use storymachine::StoryMachine;

fn read_text_file(file_name : &str) -> String {
    let mut f = File::open(file_name).unwrap();
    let mut v = String::new();
    f.read_to_string(&mut v).unwrap();
    return v;
}

fn write_txt_file(file_name: &str, text: String) {
    let mut f = File::create(file_name).unwrap();
    f.write_all(&text.into_bytes()).unwrap();
}

fn main() {
    let mut story_machine = StoryMachine::new();
    for (_, argument) in env::args().enumerate().skip_while(|y| y.0 == 0) {
        let input_text = read_text_file(&argument);
        story_machine.import_text(input_text);
    }
    if let Some(story) = story_machine.generate_story(30) {
   		write_txt_file("story.txt", story );
    }
}
