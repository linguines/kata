#[macro_use] extern crate lazy_static;
extern crate rand;
extern crate regex;

use std::collections::HashMap;
use rand::{thread_rng, Rng, ThreadRng};
use regex::Regex;

pub struct StoryMachine {
    trigrams              : HashMap<(String, String), Vec<String>>,
    sentence_starters     : Vec<(String, String)>,
    rng 		          : ThreadRng,
}

impl StoryMachine {

    pub fn new() -> StoryMachine {
        StoryMachine {
            trigrams            : HashMap::new(),
            sentence_starters   : Vec::new() ,
            rng 		: thread_rng(),
        }
    }

    pub fn import_text(&mut self, text :String) {
        lazy_static! {
            static ref CLEANER: Regex = Regex::new("[\")(]").unwrap();
            static ref ALL_CAPS: Regex = Regex::new("^[^a-z]*$").unwrap();
            static ref FIRST_LETTER_CAP: Regex = Regex::new("^[A-Z]").unwrap();
        }

        let mut bigram: (String, String) = ("".to_string(), "".to_string());
        for word in text.split_whitespace() {
            if ALL_CAPS.is_match(word) { continue; }
            let word = CLEANER.replace_all(word, "");
            if bigram.0 != "" && bigram.1 != "" {
                self.trigrams.entry( (bigram.0.clone(), bigram.1.clone() ) )
                    .or_insert(Vec::new()).push(word.clone());
                if FIRST_LETTER_CAP.is_match(&bigram.0) {
                    self.sentence_starters.push((bigram.0.clone(), bigram.1.clone()));  
                }
            }
            bigram.0 = bigram.1;
            bigram.1 = word.clone();
        }
    }

    pub fn generate_story(&mut self, paragraphs:u32) -> Option<String> {
        if self.trigrams.len() == 0 {
        	return None;
        }
        lazy_static! {
            static ref SENTENCE_ENDER: Regex = Regex::new("[.!?]").unwrap();
        }
        let mut story: String = String::new();
        for _ in 0..paragraphs {
            let mut bigram = self.get_next_sentence_starters();
            story = story +  &bigram.0.clone() + " " + &bigram.1.clone();

            for _ in 0..self.rng.gen_range(25, 200) { 
                if SENTENCE_ENDER.is_match(&bigram.1) {  
                    bigram = self.get_next_sentence_starters();
                    story = story + " " + &bigram.0.clone() + " " + &bigram.1.clone();
                }
                match self.trigrams.get( &bigram ) {
                        Some(third_word_list) => {
                            let third_word = &third_word_list[
                                    self.rng.gen_range(0, (third_word_list.len() as u32)) as usize
                                ];
                            story = story + " " + &third_word;
                            bigram = (bigram.1, third_word.clone());
                        },
                        None => { // Happens if you hit the last trigram
                            let bigrams: Vec<_> = self.trigrams.keys().collect();
                            bigram = bigrams[
                                    self.rng.gen_range(0, (bigrams.len() as u32)) as usize
                                ].clone();
                            story = story +  " " + &bigram.0.clone() + " " + &bigram.1.clone();
                        }
                };                              
            }
            story = story + ".\n\n";
        }
        return Some(story);        
    }

    fn get_next_sentence_starters(&mut self) -> (String, String) {
        if self.sentence_starters.len() > 0 {
            let sentence_starters_len = self.sentence_starters.len() as u32;
            self.sentence_starters[
                    self.rng.gen_range(0, sentence_starters_len) as usize
                ].clone()
        }
        else {
            let bigrams: Vec<_> = self.trigrams.keys().collect();
            let bigram = bigrams[
                    self.rng.gen_range(0, bigrams.len()) as usize
                ];
            (bigram.0.clone(), bigram.1.clone())
        }
    }
}
