#[macro_use] extern crate lazy_static;
extern crate regex;

use regex::Regex;

fn remove_aeiou( message : &str ) -> String {
  lazy_static! {
    static ref RE : Regex = Regex::new("(?i)[/aeiou/]").unwrap(); 
  }
  RE.replace_all(message, "")
}

fn main() {
    assert_eq!(remove_aeiou("This website is for losers LOL!"),
	       "Ths wbst s fr lsrs LL!");
}
