use std::collections::HashMap;
use std::io::prelude::*;
use std::fs::File;

fn fmt_word(word : &str) -> String {
    if word.find(' ') != None {
        format!("\"{}\"", word)
    }
    else {
        word.to_string()
    }
}

fn open_file(file_name : &str) -> String {
    let mut f = File::open(file_name).unwrap();
    let mut v = String::new();
    f.read_to_string(&mut v).unwrap();
    return v;
}

fn write_anagrams(file_name: &str, text: String) {
    let mut f = File::create(file_name).unwrap();
    f.write_all(&text.into_bytes()).unwrap();
}

fn main() {
    let mut map : HashMap<Vec<u8>, Vec<String>> = HashMap::new();
    let v = open_file("words.txt");

    for word in v.split('\n') {
        let mut key = word.to_string().replace(" ", "").into_bytes();
        key.sort();
        map.entry(key).or_insert(Vec::new()).push(fmt_word(word));        
    }

    let mut text = String::new();
    for (_, words) in &map {
        if words.len() > 1 {
            text = text + &words.join(" ") + "\n";
        }
    }
    write_anagrams("out.txt", text);
}
