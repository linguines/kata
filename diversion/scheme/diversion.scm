(require-extension regex)
(define (diversion n digits count) 
	
    (if (irregex-search "11" (number->string n 2)) 
    	(print (number->string n 2) " -->XXXX") 
    	(print (number->string n 2))
    )

	(if (= n (+ 1 (expt 2 digits))) 		
		count
		(diversion (+ n 1) digits (+ count (if (irregex-search "11" (number->string n 2)) 0 1)))
	)
)
(print (diversion 1 25 0))

