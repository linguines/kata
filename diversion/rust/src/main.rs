fn main() {
	let mut number_of_digits = 1;
	let mut count = 0;
    for x in 0..2i32.pow(25) {
    	if format!("{:b}", x).find("11") == None { 
    		count += 1;
    	}
    	if 2i32.pow(number_of_digits)-1 == x {
    		println!("Digits:{}\tCount:{}", number_of_digits, count);
    		number_of_digits += 1;   		
    	}
    }	    
}

