Code Kata #2: Knightly Communication

Knights are pretty good at being cool and wearing armor, but not so good at dialing phone numbers.  How many unique phone numbers can a knight dial?  Remember, that when a knight moves, it can go to a square that is two squares horizontally and one square vertically, or two squares vertically and one square horizontally.  So if the knight starts at 7, the next number can either be 6, or 2.


