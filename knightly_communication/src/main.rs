
struct SillyKnightWithNumericKeypad {
    keypad: Vec<Vec<usize>>
}

impl SillyKnightWithNumericKeypad {

    fn intialize_with_standard_keypad() -> SillyKnightWithNumericKeypad {
        SillyKnightWithNumericKeypad {
            keypad: vec![
                    vec![4, 6],       // 0 
                    vec![6, 8],       // 1
                    vec![7, 9],       // 2
                    vec![4, 8],       // 3
                    vec![3, 9, 0],    // 4
                    Vec::new(),         // 5
                    vec![1, 7, 0],    // 6
                    vec![6, 2],       // 7
                    vec![3, 1],       // 8
                    vec![4, 2]]       // 9,
        }
    }

    fn get_next_set_of_choices(&self,location: usize) -> Option<&Vec<usize>> {
        if self.keypad[location].len() == 0 {
            Option::None
        } else  {
            Option::Some(&self.keypad[location])
        }
    }

    fn fat_fingered_knights_unique_dialable_number_choices(&self,  digits : u32, choices: &Vec<usize>) -> usize {

        let mut total_choices = 0;
        for x in choices {
            if digits == 0 { break; }
            else if digits == 1 {
                total_choices += 1;
            }
            else {
                match self.get_next_set_of_choices(*x) {
                    None => continue,
                    Some(ref keypad_choices) => 
                        total_choices += self.fat_fingered_knights_unique_dialable_number_choices(digits-1, &keypad_choices)
                }
            }
        }
        total_choices
    }
}


fn main() {

    let knight = SillyKnightWithNumericKeypad::intialize_with_standard_keypad();
    let numerals = vec![0,1,2,3,4,5,6,7,8,9];

    println!("{}", knight.fat_fingered_knights_unique_dialable_number_choices(10, &numerals));
}
